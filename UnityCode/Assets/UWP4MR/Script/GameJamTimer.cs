﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
public class GameJamTimer : MonoBehaviour
{
    public Text m_hourValue;
    public Slider m_hour;
    public Text m_minuteValue;
    public Slider m_minute;

    public Text m_timeLeft;

    public GameObject m_panelPreference;
    public GameObject m_panelCountdown;
    [Header("Debug")]
    public string m_saveDate;
    public long m_saveDateTimestamp;



    public void AddHours(int hours) { m_hour.value += hours; RefreshDate(); }
    public void AddMinutes(int minutes) { m_minute.value += minutes; RefreshDate(); }

    public void SaveAndSwitchToCountDown()  
    {

        SaveNowDateAsInfo();
        m_panelCountdown.SetActive(true);
        m_panelPreference.SetActive(false);
    }

    public void Update()
    {
        long countDownTime =(long) (m_hour.value * 3600f + m_minute.value * 60f);
        long timeSinceRecord = GetTimestamp() - m_saveDateTimestamp;
        long time = countDownTime - timeSinceRecord;
        if (time <= 0) time = 0;
        TimeSpan t = TimeSpan.FromSeconds(time);

        int days = t.Days;
        int hours = t.Hours;
        int minutes = t.Minutes;
        int secondes=t.Seconds;

        string answer = string.Format("{0:00}h:{1:00}m:{2:00}s",
                        t.Hours + days*24,
                        t.Minutes,
                        t.Seconds);

        m_timeLeft.text = answer ;
    }
    public void SwitchToPreference()
    {
        LoadDateInfo();
        m_panelCountdown.SetActive(false);
        m_panelPreference.SetActive(true);
    }
    public void SwitchWithouSavingToCountdown()
    {
        LoadDateInfo();
        m_panelCountdown.SetActive(true);
        m_panelPreference.SetActive(false);
    }

    public void SaveNowDateAsInfo() {

        m_saveDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fffffff");
        m_saveDateTimestamp = GetTimestamp();
        PlayerPrefs.SetString("TimeSaved", "" + m_saveDateTimestamp);
        PlayerPrefs.SetInt("TimeHour", (int)m_hour.value);
        PlayerPrefs.SetInt("TimeMinute", (int)m_minute.value);
    }
    public void LoadDateInfo() {

        m_hour.value = PlayerPrefs.GetInt("TimeHour", (int)m_hour.value);
        m_minute.value = PlayerPrefs.GetInt("TimeMinute", (int)m_minute.value);

        string ts =PlayerPrefs.GetString("TimeSaved");
        if (ts != "") {
            m_saveDateTimestamp = long.Parse(ts);
            DateTime time = ConvertFromUnixTimestamp(m_saveDateTimestamp);
            m_saveDate = time.ToString("yyyy-MM-dd HH:mm:ss.fffffff");
        }
        RefreshDate();
    }

    public long GetTimestamp()
    {
        DateTime foo = DateTime.UtcNow;
        long unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();
        return unixTime;
    }

    public static DateTime ConvertFromUnixTimestamp(double timestamp)
    {
        DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        return origin.AddSeconds(timestamp);
    }

    public static double ConvertToUnixTimestamp(DateTime date)
    {
        DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        TimeSpan diff = date.ToUniversalTime() - origin;
        return Math.Floor(diff.TotalSeconds);
    }


    public void OnEnable()
    {
        m_hour.onValueChanged.AddListener(RefreshDate);
        m_minute.onValueChanged.AddListener(RefreshDate);
    }
    public void OnDisable()
    {
        m_hour.onValueChanged.RemoveListener(RefreshDate);
        m_minute.onValueChanged.RemoveListener(RefreshDate);
    }

    private void RefreshDate(float arg0)
    {
        RefreshDate();
    }

    public void RefreshDate() {

        m_hourValue.text = string.Format("{0:00}","" + m_hour.value);
        m_minuteValue.text = string.Format("{0:00}", "" + m_minute.value);

    }

    public void Start()
    {
        LoadDateInfo
            ();
    }

}
